# Andrea Pozzato - https://gitlab.com/pzzt

"-- Client Browser Migration --`n"

#$User = Read-Host -Prompt "Username"
$Server = "\\Server01\User$\" #Sostiture il percorso a seconda del cliente


$User = $env:USERNAME
$User = $User.ToLower()
"User: $User"
$RemotePath = $Server + $User + "\Desktop"
"Cartella: $RemotePath"
For ($i=6; $i -gt 1; $i--) {  
    Write-Progress -Activity "Hai 5 secondi per fermare: Ctl+C" -SecondsRemaining $i
    Start-Sleep 1
}
#####
# $RemotePath = "$HOME\Desktop\" + $User #Path di test
#####

"-"
"Firefox:"
$Folder = "$env:APPDATA\Mozilla\Firefox\Profiles"
"Check [$Folder]"
if(Test-Path -Path $Folder){
	"OK: [$Folder]"
	"-"	
	if (Test-Path -Path "$($RemotePath)\Firefox"){
		"Cartella $RemotePath esiste"
		"-"
	}else{
		New-Item -Path "$RemotePath" -Name "Firefox" -ItemType "Directory"
		"-"
	}
	"- Copy [$($Folder)\*.default-release\places.sqlite] in [$($RemotePath)\Firefox]"
	copy-item "$($Folder)\*.default-release\places.sqlite" -Destination "$($RemotePath)\Firefox"
	"- Copy [$($Folder)\*.default-release\favicons.sqlite] in [$($RemotePath)\Firefox]"
	copy-item "$($Folder)\*.default-release\favicons.sqlite" -Destination "$($RemotePath)\Firefox"
	"- Copy [$($Folder)\*.default-release\key4.db] in [$($RemotePath)\Firefox]"
	copy-item "$($Folder)\*.default-release\key4.db" -Destination "$($RemotePath)\Firefox"
	"- Copy [$($Folder)\*.default-release\logins.json] in [$($RemotePath)\Firefox]"
	copy-item "$($Folder)\*.default-release\logins.json" -Destination "$($RemotePath)\Firefox"	
}else{
	"Firefox: La path non esiste"
}

# Chrome
"Chrome:"
"-"
$Folder = "$env:APPDATA\..\Local\Google\Chrome\User Data\Default"
"Check: [$Folder]"

if(Test-Path -Path $Folder){
	"OK: [$Folder]"
	"-"	
	if (Test-Path -Path "$($RemotePath)\Chrome"){
		"Cartella $RemotePath esiste"
		"-"
	}else{
		New-Item -Path "$RemotePath" -Name "Chrome" -ItemType "Directory"
		"-"
	}
	"- Copy [$($Folder)\Login Data] in [$($RemotePath)\Chrome]"
	copy-item "$($Folder)\Login Data" -Destination "$($RemotePath)\Chrome"
	"- Copy [$($Folder)\Bookmarks] in [$($RemotePath)\Firefox]"
	copy-item "$($Folder)\Bookmarks" -Destination "$($RemotePath)\Chrome"	
}else{
	"Chrome: La path non esiste"
}


# Edge
"Edge:"
"-"
$Folder = "$env:APPDATA\..\Local\Microsoft\Edge\User Data\Default"
"Check: [$Folder]"

if(Test-Path -Path $Folder){
	"OK: [$Folder]"
	"-"	
	if (Test-Path -Path "$($RemotePath)\Edge"){
		"Cartella $RemotePath esiste"
		"-"
	}else{
		New-Item -Path "$RemotePath" -Name "Edge" -ItemType "Directory"
		"-"
	}
	"- Copy [$($Folder)\Login Data] in [$($RemotePath)\Edge]"
	copy-item "$($Folder)\Login Data" -Destination "$($RemotePath)\Edge"
	"- Copy [$($Folder)\Bookmarks] in [$($RemotePath)\Firefox]"
	copy-item "$($Folder)\Bookmarks" -Destination "$($RemotePath)\Edge"	
}else{
	"Edge: La path non esiste"
}


