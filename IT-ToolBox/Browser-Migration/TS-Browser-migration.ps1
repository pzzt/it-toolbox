#Andrea Pozzato - https://gitlab.com/pzzt

"-- TS Browser Migration --"

#$User = Read-Host -Prompt "Username"

$User = $env:USERNAME
$User = $User.ToLower()
"User: $User"
For ($i=6; $i -gt 1; $i--) {  
    Write-Progress -Activity "Hai 5 secondi per fermare: Ctl+C" -SecondsRemaining $i
    Start-Sleep 1
}

#$RemotePath = $Server + $User + "\Desktop"
$RemotePath = "$HOME\Desktop" 

"-"
"Firefox:"
$Folder = "$env:APPDATA\Mozilla\Firefox\Profiles"
"Check [$Folder]"
if(Test-Path -Path $Folder){
	"OK: [$Folder]"
    #copy-item ha dei problemi a scrivere in una path con le wildcard
    # Esempio: copy-item "$($RemotePath)\Firefox\logins.json" -Destination "$($Folder)\*.default-release\"	
    $folder_firefox = Get-ChildItem "$($Folder)\*.default-release\" 
    "OUT: $folder_firefox"
    
	"- Copy [$($RemotePath)\Firefox\places.sqlite] in [$folder_firefox]"
	copy-item "$($RemotePath)\Firefox\places.sqlite" -Destination "$folder_firefox"
	"- Copy [$($RemotePath)\Firefox\favicons.sqlite] in [$folder_firefox]"
	copy-item "$($RemotePath)\Firefox\favicons.sqlite" -Destination "$folder_firefox"
	"- Copy [$($RemotePath)\Firefox\key4.db] in [$folder_firefox]"
	copy-item "$($RemotePath)\Firefox\key4.db" -Destination "$folder_firefox"
	"- Copy [$($RemotePath)\Firefox\logins.json] in [$folder_firefox]"
	copy-item "$($RemotePath)\Firefox\logins.json" -Destination "$folder_firefox"	
}else{
	"Firefox: La path non esiste"
}

# Chrome
"Chrome:"
"-"
$Folder = "$env:APPDATA\..\Local\Google\Chrome\User Data\Default"
"Check: [$Folder]"

if(Test-Path -Path $Folder){
	"OK: [$Folder]"
	"- Copy [$($RemotePath)\Chrome\Login Data] in [$($Folder)]"
	copy-item "$($RemotePath)\Chrome\Login Data" -Destination "$($Folder)"
	"- Copy [$($RemotePath)\Chrome\Bookmarks] in [$($Folder)]"
	copy-item "$($RemotePath)\Chrome\Bookmarks" -Destination "$($Folder)"	
}else{
	"Chrome: La path non esiste"
}

# Edge
"Edge:"
"-"
$Folder = "$env:APPDATA\..\Local\Microsoft\Edge\User Data\Default\"
"Check: [$Folder]"

if(Test-Path -Path $Folder){
	"OK: [$Folder]"
	"- Copy [$($RemotePath)\Edge\Login Data] in [$($Folder)]"
	copy-item "$($RemotePath)\Edge\Login Data" -Destination "$($Folder)"
	"- Copy [$($RemotePath)\Edge\Bookmarks] in [$($Folder)]"
	copy-item "$($RemotePath)\Edge\Bookmarks" -Destination "$($Folder)"	
}else{
	"Edge: La path non esiste"
}


